<?php
if (!defined('WP_UNINSTALL_PLUGIN')) exit;

global $wpdb;

$wpdb->query("DROP TABLE IF EXISTS {$wpdb->prefix}pofw_product_option");
$wpdb->query("DROP TABLE IF EXISTS {$wpdb->prefix}pofw_product_option_value");
$wpdb->query("DROP TABLE IF EXISTS {$wpdb->prefix}pofwoptionmeta");
$wpdb->query("DROP TABLE IF EXISTS {$wpdb->prefix}pofwvaluemeta");

delete_site_option('POFW_DB_VER');
delete_site_option('POFW_VER');
