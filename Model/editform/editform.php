<?php
if (!defined('ABSPATH')) exit;

class Pektsekye_Editform_Model {

  protected $_form;


  public function __construct(){
    add_action( 'admin_menu', array($this,'add_edit_form_menu') );
  }

  public function add_edit_form_menu(){
    add_menu_page('Page title', '폼관리', 'manage_options', 'my-top-level-handle',array($this,'edit_form_options'));
    add_submenu_page( 'my-top-level-handle', 'Page title', '서브타이틀', 'manage_options', 'my-submenu-handle', array($this,'edit_form_options'));
  }

  public function edit_form_options(){
    $this->viewHtml();
  }

  public function viewHtml(){
    wp_enqueue_script('vue', 'https://cdn.jsdelivr.net/npm/vue', []);
    include_once(Pektsekye_PO()->getPluginPath() .'view/adminhtml/templates/editform/editform.php');
  }


}
