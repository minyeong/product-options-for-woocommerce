<?php
function update_option_meta($option_id, $meta_key, $meta_value, $prev_value = '') {
	return update_metadata('pofwoption', $option_id, $meta_key, $meta_value, $prev_value);
}

function update_value_meta($value_id, $meta_key, $meta_value, $prev_value = '') {
	return update_metadata('pofwvalue', $value_id, $meta_key, $meta_value, $prev_value);
}

 ?>
