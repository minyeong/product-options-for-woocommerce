<?php
if (!defined('ABSPATH')) exit;

$variable = [1,2,3,4,5,767,8,5];
?>
<?php foreach ($variable as $key => $value): ?>

<div id="app" class="app">
  {{ message }}
</div>
<?php endforeach; ?>
<script type="text/javascript">
jQuery(document).ready(function(){
  var app = new Vue({
    el: 'app',
    data: {
      message: '안녕하세요 Vue!'
    }
  })
})
</script>
