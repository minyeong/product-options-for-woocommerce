=== Simple Product Options for WooCommerce ===

Contributors: pektsekye
Tags: lenses prescription, customer order note, dependent variations, price calculation formula,  
Requires at least: 4.7
Tested up to: 4.9
Stable tag: 1.0.0
License: GPLv2 or later
License URI: http://www.gnu.org/licenses/gpl-2.0.html


It adds drop-down, radio button and text field options on the product page.


== Description ==

This plugin adds selectable options to WooCommerce products.
So customer can select size, color and specify custom text before adding a product into the shopping cart.
The store administrator can create product options on the edit product page.
He can set price for each option value and make options required "mandatory" for selection.
So the customer will not be able to add the product the shopping cart without selecting the required options.
Selected values are saved and then displayed on checkout pages and in the order complete emails.


**Main Features:**

* Drop-down, radio button, multiple select, checkbox, text field and text area option types.
* Set price for each option value.
* Make options mandatory for selection.
* Display selected values on checkout, order info pages and in the order emails.


**Main Limitations:**

* The current version of this extension supports only Simple products.
* It uses JavaScript to change product price so it is very dependent on theme's HTML. If it cannot find and update the product price you will need to adjust the JS file.


You can check a demo page here:
**[DEMO Page](http://hottons.com/demo/wp/as/product/dayton-dt30d/)**

You can read the installation instructions here:
**[README](http://hottons.com/demo/wp/po/README.html)**

This plugin has just 19 files to make it easy to use and customize. If you need more features check the Modifications section on this page.

Contact me by email <pektsekye@gmail.com> if you have questions or need help.


== Screenshots ==

1. Custom Options section on the product edit page.
2. Product options on front-end.
3. Selected values on checkout page.


== Installation ==

Requires WordPress 4.7 and WooCommerce 3.0 or greater.

1. Unzip the downloaded zip file.
2. Upload the plugin folder into the `wp-content/plugins/` directory of your WordPress site.
3. Activate `Product Options for WooCommerce` from Plugins page.

Contact me by email <pektsekye@gmail.com> if you have questions or need help.


== Changelog ==

= 1.0.0 - Released: Jun, 10 - 2018  =

* Initial release


== Modifications ==

= List of available modifications: =

* Option note, description fields and CSV export-import
  Options with HTML note or description text and CSV export-import feature.
  [screenshot](http://hottons.com/demo/wp/po/screenshots/modifications/note_description_csv_1.png) download modified plugin: [pofw_1.0.0_with_note_description_csv.zip](http://hottons.com/demo/wp/po/screenshots/modifications/pofw_1.0.0_with_note_description_csv.zip)

* Dependent options
  Make product options dependent on each other by adding special words to the option titles.
  [screenshot 1](http://hottons.com/demo/wp/po/screenshots/modifications/dependent_options_1.png) [screenshot 2](http://hottons.com/demo/wp/po/screenshots/modifications/dependent_options_2.png) download modified plugin: [pofw_1.0.0_with_dependent_options.zip](http://hottons.com/demo/wp/po/screenshots/modifications/pofw_1.0.0_with_dependent_options.zip)

* Lenses prescription
  It will display prescription options in a row. You can import all options as a .csv file.
  [screenshot 1](http://hottons.com/demo/wp/po/screenshots/modifications/lenses_prescription_2.png) [screenshot 2](http://hottons.com/demo/wp/po/screenshots/modifications/lenses_prescription_4.png) download modified plugin: [pofw_1.0.0_with_lenses_prescription.zip](http://hottons.com/demo/wp/po/screenshots/modifications/pofw_1.0.0_with_lenses_prescription.zip) download sample CSV file: [lenses_prescription.csv](http://hottons.com/demo/wp/po/screenshots/modifications/lenses_prescription.csv)

* Square meter price
  Customer will enter his width and height values into the text field options. This modification will calculate square meter price and will add it to the product price.
  [screenshot 1](http://hottons.com/demo/wp/po/screenshots/modifications/square_meter_price_1.png) [screenshot 2](http://hottons.com/demo/wp/po/screenshots/modifications/square_meter_price_3.png) download modified files and instructions: [square_meter_price.zip](http://hottons.com/demo/wp/po/screenshots/modifications/square_meter_price.zip) download this plugin with all files modified: [pofw_1.0.0_with_square_meter_price.zip](http://hottons.com/demo/wp/po/screenshots/modifications/pofw_1.0.0_with_square_meter_price.zip)


Full list of Product Options modifications [hottons.com/po_modifications](http://hottons.com/po_modifications)


== Other plugins ==

= If you like this plugin check also: =

* [Dependent Custom Options (gallery)](http://hottons.com/woocommerce/wp-ox.html) (PAID)
  If you need options with images and descriptions and to make them dependent on each other.


== Translators ==

= Available Languages =
* English (Default)


== Documentation ==

Full documentation is available [here](http://hottons.com/demo/wp/po/README.html).